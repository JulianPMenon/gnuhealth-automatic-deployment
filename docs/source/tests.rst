.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Tests
=====

Numerous tests are defined in the `tests` folder and called from `.gitlab-ci.yml`.
Some tests are simple BASH scripts and used for:

- Checking if the original configuration files used for templates are up to date

- Linting Ansible code

Other tests are build on Molecule, VirtualBox and Vagrant.
GNU Health and Orthanc are tested for all-in-one-systems and GNU Health is also tested for separating database, application and reverse proxy.
In any case every operating system listed in the introduction is tested. Molecule also performs syntax and idempotency checks.
Integration tests were added as well: They test if the connection works when sending a request to the reverse proxy that has to be processed by the application and the database as well.

Additionally it is tested if the project fulfills the REUSE compliance.
This happens directly inside the `.gitlab-ci.yml` as it is only one line needed (`reuse lint`).

Currently the molecule tests are not triggered by commits because they require a shell executor and this implies security issues.
But they are executed periodically.

| REUSE by the Free Software Foundation Europe:
| https://reuse.readthedocs.io/en/latest/

| Molecule for testing Ansible:
| https://molecule.readthedocs.io/en/latest/
