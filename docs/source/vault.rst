.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Encryption by Ansible-Vault
===========================
Passwords or private keys should not be stored in plain text unencrypted.
You can encrypt the files containing passwords like this::

    $ ansible-vault encrypt inventories/<inventory>/group_vars/gnuhealth/vault.yml

Afterwards the playbook has to be executed with the following flag (and maybe more, see Examples)::

    $ ansibe-playbook playbooks/gnuhealth.yml --ask-vault

If you access the target machine from remote but have the private key for the certificate on the system running Ansible, encrypt it as well::

    $ ansible-vault encrypt /path/to/key.pem

Having two encrypted files - `vault.yml` and the key - the playbook has to be run in the following way::

    $ ansible-playbook playbooks/gnuhealth.yml --vault-id vault@prompt --vault-id key@prompt

You can edit an encrypted file like this::

    $ ansible-vault edit inventories/<inventory>/group_vars/gnuhealth/vault.yml
