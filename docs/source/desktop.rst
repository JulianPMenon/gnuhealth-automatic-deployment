.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Desktop - workstation
=====================

| The desktop deployment mainly contains the GNU Health client.
| The client itself will be installed using zypper for openSUSE and using pip inside a virtual environment using Ubuntu/Debian.
| Besides the connection informations for the server and databases are already entered in the config.
  Those informations are mostly taken from the HMIS node setup. If `ghcl_add_community_server` is true an entry for the community server will be created as well.
  This configuration and the plugin will be placed in /etc/skel/ in order to show up for newly created operating system users.
  If `ghcl_copy_config` is true it will also be copied to the home directory of the current user set in `vault.yml`.
| The client can be executed using the desktop entry or running `gnuhealth-client` inside the terminal.
  By default the password for the admin user is `gnusolidario`. It is set in `group_vars/gnuhealth/vault.yml` in the inventory.

For using the crypto plugin a GPG key is necessary.
If you want to create one set `gpg_create_key` true and set the GPG related values below.

The client does not have an option to ignore security risks for unknown certificates.
If you created a custom CA, set `ghcl_trust_certs` to true in order to trust the certificate.

Finally you can also install MyGNUHealth but this is not constantly tested and does not work on all distributions.
