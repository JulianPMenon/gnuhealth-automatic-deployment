# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

---
# tasks file for ssh
- name: Set SSH port
  ansible.builtin.lineinfile:
    path: /etc/ssh/sshd_config
    line: "Port {{ ssh_port }}"
    regexp: "^(.*)Port (.*)$"
  notify: Reload sshd
- name: Ensure local SSH key exists
  community.crypto.openssh_keypair:
    path: "{{ ssh_key_path }}"
    force: false
    regenerate: never
    type: rsa
  when: ssh_key_create
  delegate_to: localhost
  become: false
- name: Get content of local SSH key
  ansible.builtin.command: "cat {{ ssh_key_path }}.pub"
  register: ssh_key_path_content
  changed_when: false
  delegate_to: localhost
  when: ssh_key_add_from_path
  become: false
- name: Add SSH key from path
  ansible.posix.authorized_key:
    key: "{{ ssh_key_path_content.stdout }}"
    user: "{{ ssh_username_remote }}"
  notify: Reload sshd
- name: Add SSH keys from strings
  ansible.posix.authorized_key:
    key: "{{ item }}"
    user: "{{ ssh_username_remote }}"
  notify: Reload sshd
  loop: "{{ ssh_key_add_from_strings }}"
- name: Disable password based SSH access
  ansible.builtin.lineinfile:
    path: /etc/ssh/sshd_config
    line: "PasswordAuthentication no"
    regexp: "^(.*)PasswordAuthentication(.*)$"
  when: ssh_disable_pw_auth
  notify: Reload sshd
