.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Troubleshooting
===============

* If the GNU Health task "initialize database in tryton" fails on installation but is skipped afterwards, the check got broken and you can fix it by deleting the database "health".
* PostgreSQL needs the server running for some tasks. If you made it fail it might be necessary to perform the fixing changes first manually and then inside Ansible.
  Otherwise you would either have the playbooks failing because it's not running or break it again after you fixed it manually.
* Some tasks have `no_log` set to true for not having passwords that have to be set in the logs. If such a task fails it makes debugging quite complicated.
  In this scenario look for the task and set `no_log` to true if necessary. Change the password afterwards if it was present in the logs.
* For debugging uncomment lines in ansible.cfg and run the playbook with -vvv option. You can also set `display_skipped_hosts` to true for displaying skipped tasks.
* In case of error “WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!” e.g. after rebuilding a system you already connected to, run::

    $ ssh-keygen -R <IP/DN>
