# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

nginx: true
nginx_rproxy_domain: localhost  # for certificate & reverse proxy
nginx_application_domain: "{{ nginx_rproxy_domain }}"
nginx_http_redirect: false
nginx_sites:
  - {name: 'GH_HMIS', internal_port: '8443', external_port: '443', unix_socket_path: '/opt/gnuhealth/var/run/gnuhealth.sckt'}

nginx_cert_filename: "{{ cert | default('placeholder') }}"
nginx_key_filename: "{{ key | default('placeholder') }}"

nginx_application_https: "{{ application_https | default(false) }}"

nginx_uwsgi: "{{ uwsgi_config is defined }}"
nginx_uwsgi_unix_socket: "{{ not nginx_application_https and nginx_uwsgi and nginx_application_domain == 'localhost' }}"

nginx_copy_app_cert: "{{ cert_application.type == 'ownca' or cert_application.type == 'copy' }}"
# yamllint disable-line rule:line-length
nginx_app_cert_path: "{{ nginx_app_cert_path_ownca if cert_application.type == 'ownca' else (nginx_app_cert_path_local if cert_application.type == 'copy' and not cert_application.remote_source else nginx_app_cert_path_remote) }}"
nginx_app_cert_path_ownca: "../fetch/{{ cert_ownca.cert_params[-1] | default('/path/placeholder') }}.crt"
nginx_app_cert_path_local: "{{ cert_application.my_cert | default('/path/placeholder') }}"
nginx_app_cert_path_remote: "../fetch/{{ (cert_application.my_cert | basename) | default('/path/placeholder') }}"
nginx_copy_crl: "{{ cert_application.type == 'ownca' | default(false) }}"
nginx_crl_path: "../fetch/{{ cert_ownca.cert_params[-1] | default('/path/placeholder') }}.crl"
nginx_ownca: "{{ nginx_copy_crl | default(false) }}"

nginx_fail2ban: "{{ msc_fail2ban | default(false) }}"

nginx_letsencrypt: "{{ cert_rproxy.type == 'letsencrypt' | default(false) }}"

nginx_package_version_pinning: false
nginx_package_version: '1.18.0-6ubuntu14.3'

nginx_template_comment: "Managed by Ansible, do not edit manually!"
