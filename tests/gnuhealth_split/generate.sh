#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

echo "Current working directory and its content:"
pwd
ls

cp -r inventories/dev/group_vars tests/gnuhealth_split/molecule/default/

sed -i 's/psql_domain: .*/psql_domain: db/' tests/gnuhealth_split/molecule/default/group_vars/gnuhealth/vars.yml
sed -i 's/nginx_application_domain: .*/nginx_application_domain: app/' tests/gnuhealth_split/molecule/default/group_vars/gnuhealth/vars.yml
sed -i 's/nginx_rproxy_domain: .*/nginx_rproxy_domain: web/' tests/gnuhealth_split/molecule/default/group_vars/gnuhealth/vars.yml
sed -i 's|.*- "hostssl.*|  - "hostssl\t{{ psql_db_name }},template1\t\t{{ psql_db_user }}\t192.168.60.101/32\t\tscram-sha-256"|' tests/gnuhealth_split/molecule/default/group_vars/gnuhealth/vars.yml

diff -r inventories/dev/group_vars/gnuhealth/vars.yml tests/gnuhealth_split/molecule/default/group_vars/gnuhealth/vars.yml 2>&1 | tee tests/gnuhealth_split/diff
