.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Securing SSH & passwords
========================
If your server accepts SSH connections, the playbooks work directly if you specify the right domain/IP, username and
password. However you could consider securing SSH:

Enable host key checking
------------------------
* Change `vault_ssh_args` to '-o StrictHostKeyChecking=yes'

* Add the hosts fingerprint to known_hosts in ssh::

    $ ssh-keyscan -H domain >> ~/.ssh/known_hosts

* Login to the server and make sure the fingerprints match


Disable password-based ssh login and root login
-----------------------------------------------
* Generate a ssh key if necessary::

    $ ssh-keygen -t rsa

* Copy the public key to the server::

    $ ssh-copy-id -i ~/.ssh/key_rsa.pub user@server

  (check in ~/.ssh/authorized_keys at server)

* To disable password-based login and root login edit /etc/ssh/sshd_config as sudo:

::

    ChallengeResponseAuthentication no
    PasswordAuthentication no
    UsePAM no
    PermitRootLogin no

* You might need to setup ssh-agent for ansible because it does not prompt for a password to unlock the private key (or try to make it prompt for the password)::

    $ ssh-agent bash
    $ ssh-add ~/.ssh/id_rsa (ssh private key)
    $ exit

* Try connecting::

    $ ansible gnuhealth -m ping -e "ansible_user='user'"

Handling the sudo password
--------------------------
If you use a password stored in vault you could at least encrypt the vault file like it is
explained in the section `Encryption by Ansible-Vault`.

In order to avoid putting your sudo password in vault two different approaches for becoming sudo are tested:

* Set `vault_set_sudo_pass` to false and run the playbook with the `-K` flag (default)

* Enable passwordless sudo on the target system and set `vault_set_sudo_pass` to false as well.
  For example `sudo visudo /etc/sudoers/` and change the line `%sudo   ALL=(ALL:ALL) ALL`
  to `%sudo  ALL=(ALL:ALL) NOPASSWD: ALL` on Ubuntu. For openSUSE use `wheel` instead of `sudo` and add your user to
  wheel: `sudo usermod -aG wheel username`.

Prompt for SSH password
-----------------------

If you do not have a SSH key but want to avoid putting the password in `vault.yml` use the `-k` flag when calling the playbook.
Additionally you have to remove the line `- ansible_ssh_pass: "{{ auth_ssh_password }}"` from the playbook gnuhealth.yml or another one.

Using the SSH playbook and role
-------------------------------

There is a playbook, a role and configuration if you want to make Ansible handle your SSH setup.

First of all decide which hosts should be affected. Therefore set the groups or hosts in inventory/hosts on the bottom under [ssh].

For the default configuration you need to set `ssh_username_remote` and `ssh_username_local` and use the -K and -k flags::

    $ ansible-playbook playbooks/ssh.yml -i inventories/dev -e ssh_username_remote=<username> -e ssh_username_local=`whoami` -K -k

This will create a SSH key pair for your local user if noone exists, add its public key or an already existing one with default path to the server and disable password based authentication.

Afterwards - if you want to test the connection or proceed with other playbooks - you will need to add the key to SSH agent like it is described above or set the corresponding flag when calling ansible-playbook::

    $ ansible-playbook playbooks/ssh.yml -i inventories/dev -e ssh_username_remote=<username> -e ssh_username_local=`whoami` -K --key-file /home/`whoami`/.ssh/id_rsa

Note that Ansible might shortly save the old connection. While testing the above parameter became necessary after a minute.

Furthermore you can change the configuration in your inventories group_vars/ssh/vars.yml:

* `ssh_username_remote` sets the uesrname of the remote system

* `ssh_port` sets the TCP port (if changed, change it in inventory/hosts or whereever connection is configured afterwards)

* `ssh_key_create` controls if a local SSH key is created

* `ssh_username_local` sets the username of the local system / ansible controller (only used for default value of `ssh_key_add_from_path`)

* `ssh_key_add_from_path` controls if a local SSH key is added to the server

* `ssh_key_path` sets the path for both `ssh_key_create` and `ssh_key_add_from_path`

* `ssh_key_add_from_strings` expects a (possibly empty) list of public SSH keys as strings that are meant to be trusted by the server
