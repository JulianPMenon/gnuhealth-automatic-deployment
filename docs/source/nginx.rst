.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Nginx
=====

Nginx is used as reverse proxy / web server.
Where it is optional to have HTTPS for the application and the database this is the part of your system to be accessed from outside.
Thus it will always use HTTPS and create a custom CA if you do not provide official certificates on your own.

Basically the nginx package is installed and a site template for public access is used.
For uWSGI it might be necessary to add the Nginx user to the `uwsginginx` group.

Nginx will only be accessible using TLS 1.2 & 1.3 because older versions have known security issues.

Apart from setting paths for certificate and keys for Nginx itself and the application (for verification) the site template contains numerous connection options for accessing the application:

- In case of GNU Health:

  - Using a UNIX socket if Nginx and GNU Health are on the same system

  - Using the uWSGI protocol if having separate systems but the intranet is considered secure (or performance considered too poor)

  - Using HTTPS if having separate systems but internal communication should be encrypted as well (default for separate systems)

- In case of Orthanc:

  - HTTP if it's the same system

  - HTTPS by default if having separate systems

The domain names `nginx_rproxy_domain` and `nginx_application_domain` have to be set correctly:
The first one is used for the certificate and the second one is where the application server is expected.

If setting `nginx_http_redirect` a redirect from HTTP to HTTPS will be created as well.

In order to modify or extend the Nginx sites configuration have a look at the templates in `roles/nginx/templates/`.

| Nginx documentation:
| https://docs.nginx.com/
