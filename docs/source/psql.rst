.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

PostgreSQL
==========

By default PostgreSQL is used for all servers and the same role is shared for it.
This is optional for Orthanc. There you could also disable this by setting `or_use_postgresql` to false. Then SQLite would be used.
Technically the Tryton kernel used by GNU Health also supports other backends but PostgreSQL is strongly recommended thus this is not configurable inside this project.

| The postgresql role installs the necessary packages and creates a PostgreSQL user and a database for further use.
| Configuration options can be found in `inventories/dev/group_vars/gnuhealth/vars.yml` prefixed by `psql_`.
  For example encoding, locale and timezone can also be set.
| In the chapter `Project Structure` it was already described how to separate systems from each other.
  If PostgreSQL is on a separate system, the database user should have a password and the connection should be encrypted.
  By default a certificate for TLS (`psql_use_cert`) and a scram-sha-256 password (`psql_use_pw`) is created if the variables `psql_domain` and `nginx_application_domain` are not identical.
| The list `psql_hba_rules` contains one rule for local UNIX socket access.
  For remote access uncomment the second line and set the right IP address. It enables remote access for an IP (range), scram-sha-256 passwords, TLS and your newly created database user.
  This way the risk of undesirable access is reduced.
| Finally `shared_buffers` are set to 40% of available memory which is recommended for performance tuning.

On application level the default URI already contains parameters for verifying the TLS connection (see `gh_psql_uri_remote`).
Generally there are three different ways for applications to communicate to PostgreSQL:

- UNIX socket (local)

- TCP port 5432, unencrypted (remote, forbidden by default)

- TCP port 5432, encrypted (remote)

Unencrypted remote access is forbidden by the line in `psql_hba_rules`.
Changing `hostssl` to `host` allows both encrypted and unencrypted access.

| PostgreSQL documentation:
| https://www.postgresql.org/docs/14/index.html
