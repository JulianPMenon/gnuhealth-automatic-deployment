.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Digital certificates
====================

In order to realize encrypted communication using TLS/SSL digital certificates are needed.
The frontend Nginx will always use HTTPS.
If the application and database are on different systems those subsystems will also communicate encryptedly with each other by default.
Three different ways of providing certificates are supported:

- **copy**: Simply setting a path of existing certificate and key.

- **letsencrypt**: Issuing certificate and private key using Lets Encrypt.

- **ownca**: Creating a custom Certificate Authority (CA) that signs certificates of the servers using OpenSSL. This is the default because it is possible without public accessibility.

This refers to setting the `type` for the certificate configuration in the inventories to `copy`, `letsencrypt` or `ownca`.
In `inventories/dev/group_vars/gnuhealth/vars.yml` you will find dictionaries `cert_rproxy`, `cert_application` and `cert_psql` containing values like `type` that are passed to the certificate role.
Depending on the type additional values can be set as well:

- For `copy` you have to set the two paths for your certificate and key.
  The boolean `remote_source` states if those are on the Ansible controller or on the target system.
  The certificate is expected to contain both the chain and the certificate itself. If it's not already in that format, you can create it like this::

    $ cp cert.pem cert-chain.pem
    $ cat chain.pem >> cert-chain.pem

- `letsencrypt` is only supported for Nginx because the application and database system should not have publicly open ports.
  The `nginx_rproxy_domain` of the Nginx configuration will be used as domain name and the `cert_admin_mail` inside the already mentionned dictionaries as email.
  If necessary you can set `revoke` to true in order to revoke and delete the certificate.
  Until here Let's Encrypt was only tested on Ubuntu and is not part of the automated tests because it requires public accessibility.

- The default `ownca` creates a local CA. The servers using TLS will all create a private key and a certificate signing request (CSR).
  This CSR will be transferred to the CA which creates and signs the actual certificate to be transferred back to the servers (if you actually specify separate systems).
  While the certificate role is called implicitly by every server the CA obviously has to be created before. Hence this is the very first step.
  For this option parameters like file name and geographical informations are passed during the certificate creation.
  The Common Name (CN) is taken from other variables by default (`nginx_rproxy_domain`, `nginx_application_domain` or `psql_domain` depending on the server type).
  The CA holds an index of created certificates and certificate revocation lists (CRL) are distributed to the application and the reverse proxy Nginx.
  If you fill the list cert_ownca.revoke with certificates to be revoked those will be part of the CRL and connecting to them will be refused.
  Unfortunatelly there is no functionality yet to use this kind of CRL for the work stations.
  When requesting an official certificate revocation is handled in a more secure way because the information which certificates are revoked can always be accessed and does not need to be distributed by calling this Ansible playbooks.
  This could be realized by setting up a web server for OCSP to enable real time checks on the CAs revocation lists.

Some paths where to put TLS/SSL related files and validity ranges for a custom CA are configured in the certificate roles' vars & defaults directories in the dictionaries `cert_ssl_paths` and `cert_ownca_params`.
By default keys are stored in /etc/ssl/private and certificates in /etc/ssl/certs but need to be copied for PostgreSQL, GNU Health and Orthanc.
Of course the keys will never be world-readable.


| OpenSSL documentation:
| https://www.openssl.org/docs/
|
| Let's Encrypt documentation:
| https://letsencrypt.org/docs/
