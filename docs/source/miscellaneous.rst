.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Miscellaneous
=============

There are some additional features you can use for your servers that are part of the `miscellaeneous` role.
Those are all prefixed by `msc_` in the servers `vars.yml` configuration files:

Fail2Ban
--------
Fail2Ban monitors malicious network behaviour in log files and bans IPs temporarily if their behaviour matches specific filters (putting IPs in so called "jails").
After installation it's only enabled for ssh. The scripts enable some filters for nginx as well.
Banning IP addresses is controlled by `msc_fail2ban_banaction` which is `iptables-allports` by default.
You can search for bans with the following command:

`sudo zgrep 'Ban' /var/log/fail2ban.log*`

https://www.fail2ban.org/

Automatic updates
-----------------
If `msc_automatic_updates` and `msc_automatic_reboot` are true as well, automatic updates and reboots are enabled.
On Debian/Ubuntu Unattended Upgrades realize this and for openSUSE this feature is disabled because the community does not recommend to use automatic upgrades.
For sure reboots should not be enabled in a system that has to be running 24/7. If enabling reboots they will by default be performed at 2am if necessary (configurable using `msc_automatic_reboot_time`).

https://wiki.debian.org/UnattendedUpgrades

Monitoring systemd services
---------------------------
In group_vars/gnuhealth/vars.yml you can set systemd services that should be monitored.
For everyone of those services a drop-in config file will be added in order to trigger emails when the service is stopped.

E-mail notifications
--------------------
The option to send emails can be realized using sSMTP.
However this is not optimal since it requires to transfer your password into /etc/ssmtp/ssmtp.conf which can be read by users with sudo rights.
Thus you might want to find a different way to enable the "sendmail" command yourself - for example using postfix.
If you want to use sSMTP, create a new mail account for this and make sure that access from applications is allowed.
It's tested using web.de which requires to "allow IMAP/POP3" although only SMTP will be used.
Installation of sSMTP is controlled by the boolean `msc_ssmtp`.
Sending mails for Fail2Ban or services depends on `msc_fail2ban_use_mail` and `msc_email_on_service_inactivity`.


Timezone
--------
Finally you can set a timezone in order to have the same in all your machines. This will trigger a restart of systemd-timesyncd and cron.
