.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Project structure
=================

The deployment of GNU Health is automated in a modular way.
Thus you can create Virtual Machines (VMs) in order to have one distinct system per functionality:

.. image:: graphics/overview.png

This is realized using the automation and configuration management tool **Ansible**.

For every type of deployment there is one `.yml` file in the **playbooks** directory.
Those are the main scripts that you can execute.

Additionally there are Ansible roles with the corresponding names and other **roles** getting called implicitly.
They are containing the tasks that will install packages and perform configuration steps for creating the desired service.

| Your individual configuration is managed in the different **inventories** folders.
| They can be used for having different develop, test & production environments.
  In this repository only the `dev` inventory is filled.
  It can be used as a base for a test or prod environment.
| Inside `dev` is a subfolder `group_vars` where you will find directories for different deployments each containing two configuration files `vars.yml` and `vault.yml`.
  The first one contains the main configuration and the second one credentials.
  Thus it is meant to be encrypted using Ansible Vault (see Securing SSH & PWs).
| Some default variables are only stored in roles/<name>/defaults/main.yml. Usually they do not have to be modified but especially if you want to reuse single roles you
  will need them.
| Finally the connection informations of the target hosts are given in `inventory/hosts` inside inventories folders.

The **vagrant** folder contains Vagrantfiles and inventory files for overriding the `hosts` file mentionned above.
Those can be used to set up VMs using VirtualBox for different scenarios.

If files are needed by different systems those are put into the **fetch** folder.
For some cases of handling certificates files have to be transferred from one system to another.
Besides the desktop systems having the GNU Health client installed need connection parameters of the server.
Those are also written into this folder.

Finally there are **tests** that are constantly used and **docs** which you are currently reading.

Until here it is possible to split your GNU Health (or Orthanc) system into the actual application, PostgreSQL, Nginx and a custom CA.
If you want all of them to be different systems your `hosts` file should be modified to match the following snippet.
Replace the <placeholders> by your actual domain name or IP address.
The variable `ansible_port` is put so you could set a custom SSH port.
By default there is one uncommented entry in the top level group leading to all in one system.

::

    [gnuhealth]
    # outcommented ansible_port=22

    [gnuhealth:children]
    gnuhealth_application
    gnuhealth_psql
    gnuhealth_rproxy
    gnuhealth_ownca

    [gnuhealth_application]
    <domain> ansible_port=22

    [gnuhealth_psql]
    <domain> ansible_port=22

    [gnuhealth_rproxy]
    <domain> ansible_port=22

    [gnuhealth_ownca]
    <domain> ansible_port=22

You can modify `ansible.cfg` in the top level directory if you wish to change your Ansible configuration options.

The following graphic shows the main steps of execution:

.. image:: graphics/ansible_flow.png
