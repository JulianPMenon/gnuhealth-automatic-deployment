# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

from proteus import config
print('Connecting to the server...')
conf = config.set_xmlrpc("https://admin:gnusolidario@localhost/health/")
print('Success')
