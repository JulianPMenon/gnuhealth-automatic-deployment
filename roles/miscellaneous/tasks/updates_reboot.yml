# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Configure unattended-upgrades
- name: Configure unattended-upgrades
  notify: Restart and enable unattended-upgrades
  when: ansible_facts.os_family == 'Debian' and msc_automatic_updates
  block:
    - name: Install unattended-upgrades and list changes
      ansible.builtin.apt:
        name: [unattended-upgrades, apt-listchanges]
    - name: Apply template for unattended-upgrades
      ansible.builtin.template:
        src: 50unattended-upgrades.j2
        dest: /etc/apt/apt.conf.d/50unattended-upgrades
        mode: '0644'
    - name: Apply another template for unattended-upgrades
      ansible.builtin.template:
        src: 20auto-upgrades.j2
        dest: /etc/apt/apt.conf.d/20auto-upgrades
        mode: '0644'
- name: Check if service exists
  ansible.builtin.command: "systemctl cat unattended-upgrades"  # noqa command-instead-of-module
  check_mode: false
  register: service_exists
  changed_when: false
  failed_when: service_exists.rc not in [0, 1]
  when: ansible_facts.os_family == 'Debian' and not msc_automatic_updates
- name: Disable unattended-upgrades
  ansible.builtin.systemd:
    name: unattended-upgrades
    state: stopped
    enabled: false
  when: ansible_facts.os_family == 'Debian' and not msc_automatic_updates and service_exists.rc == 0

# Not implemented on openSUSE
- name: Automatic upgrades and reboot (suse)
  ansible.builtin.debug:
    msg: "automatic upgrades not recommended thus not implemented for openSUSE"
  when: ansible_facts.os_family == 'Suse' and msc_automatic_updates
