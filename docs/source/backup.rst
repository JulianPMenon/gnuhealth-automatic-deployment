.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Backups
=======

There is no backup functionality integrated yet.
For your backup/restore strategy you should at least take into account the following:

- This project before and after your modifications

- If using a custom CA, its certificate and key, best the whole folder (/root/tls by default)

- PostgreSQL: Database

- GNU Health: Attachments folder (/opt/gnuhealth/var/lib by default)

- Private keys and certificates if necessary

Follow these links for further informations for backup & restore:

https://en.wikibooks.org/wiki/GNU_Health/Control_Center

https://book.orthanc-server.com/users/backup.html

https://www.postgresql.org/docs/devel/backup-dump.html

https://www.postgresql.org/docs/14/continuous-archiving.html
