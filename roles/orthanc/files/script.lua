function IncomingHttpRequestFilter(method, uri, ip, username, httpHeaders)
   -- allow GET for alice, everything but DELETE for bob and everything for admin 

  if username =='alice' then
    if method == 'GET' then
      return true
    else
      return false
    end
  elseif username == 'bob' then
    if method == 'DELETE' then
      return false
    else
      return true
    end
  elseif username == 'admin' then 
    return true 
  else 
    return false 
  end
end
