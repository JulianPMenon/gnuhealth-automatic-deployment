# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

# -*- mode: ruby -*-
# vi: set ft=ruby :

# This installs the Orthanc server in one system using VirtualBox.
# Orthanc can be accessed from a browser navigating to https://localhost


Vagrant.configure("2") do |config|
  config.vm.box = "gusztavvargadr/ubuntu-desktop"
  ENV['ANSIBLE_ROLES_PATH'] = "../../roles"
  config.ssh.insert_key = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provider :virtualbox do |v|
    v.memory = 1024
    v.linked_clone = true
  end

  config.vm.define "gui" do |gui|
    gui.vm.hostname = "gui"
    gui.vm.network :private_network, ip: "192.168.60.130", virtualbox__intnet: true, virtualbox__intnet: "vagrantnet"
    gui.vm.network "forwarded_port", id: "ssh", host: 2230, guest: 22
    gui.vm.provision "ansible" do |ansible|
      ansible.playbook = "../../playbooks/orthanc.yml"
      ansible.raw_arguments = [
        "--inventory=../../inventories/dev",
        "--inventory=./hosts"
      ]
      ansible.extra_vars = {
        psql_db_template: "template0",
        psql_set_locale: true,
        psql_set_timezone: true,
        psql_set_datestyle: true
      }
    end
  end
end
