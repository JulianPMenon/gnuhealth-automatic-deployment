.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

GNU Health HMIS node
====================

The playbook `gnuhealth.yml` refers to the HMIS node which is the core of GNU Health.
Its role `gnuhealth` is the only role yet that has its separate repository:

https://gitlab.com/geraldwiese/gnu-health-ansible-role


Main Installation
-----------------

First of all a new operating system user `gnuhealth` without login shell and SSH access is created.
By default the following directories are created:

- `/opt/gnuhealth` -- home folder

- `/opt/gnuhealth/etc` -- config folder

- `/opt/gnuhealth/var/log` -- log folder

- `/opt/gnuhealth/var/lib` -- data folder

- `/opt/gnuhealth/var/run` -- run folder

- `/opt/gnuhealth/tls` -- TLS folder

- `/opt/gnuhealth/venv` -- virtual python environment

The run folder contains the UNIX socket if GNU Health & Nginx are on the same system.

In case of separate systems a certificate and key will be stored in the TLS folder for enabling HTTPS.
The certificate and CRL of PostgreSQL will also be transferred in this directory for verification.

GNU Health itself is installed using the PyPI package `gnuhealth-all-modules` inside the virtual environment.
If using openSUSE Python 3.9 will be installed and used for it in order not to use Python 3.6 which has reached end of life.

After the installation the database created by PostgreSQL before will be initialized using Tryton.
Besides the modules specified in `gh_modules` are activated.

Configuration options for GNU Health itself are prefixed by `gh_`. Templates in use can be found in `roles/gnuhealth/templates`.
The `trytond.conf` is based on the one shipped by zypper on openSUSE.

The application will be executed using uWSGI (see next chapter).


Additional steps
----------------

Optionally you can also add the demo database to your system by setting `gh_demo_db` to true.

For using the calendar set `gh_webdav` true as well.

The modules are all installed but by default only the core module `health` is activated in the database.
You can also extend the list `gh_modules`. Note that you will also get the depending ones activated.

If you let the playbooks install Orthanc using a custom CA and want to connect Orthanc to the HMIS you can trust this CA by setting `gh_trust_cert_orthanc` to true and specifying the path.

In the past GNU Health already shipped some aliases for easing the process of remote support.
Those are mainly overtaken and some modified.
They are shipped as .gnuhealthrc which is now activated by the .bashrc of the `gnuhealth` user:

- `activate`: Activates the virtual python environment (makes former cdexe obsolete)

- `cdlogs`: Change to the directory where log files are stored

- `cdconf`: Change to the config directory

- `cdmods`: Change to the directory containing GNU Health modules

- `editconf`: Edit the main config file trytond.conf

If you set `gh_update_weekly` to true, a BASH script is shipped that will automatically run minor updates on GNU Health using pip.

Additionally `gh_restart_service_after_update` can be set to true to trigger a service restart after an update.
In a test environment uWSGI was able to restart its service without a break of accessibility (having 10 connectivity tests per second).
Nevertheless this should not be enabled for productive systems without further individual tests.


Other installation methods
--------------------------

If you want to compare this to other installation strategies have a look at Wikibooks

https://en.wikibooks.org/wiki/GNU_Health/Installation

or openSUSE

https://en.opensuse.org/GNUHealth_on_openSUSE
