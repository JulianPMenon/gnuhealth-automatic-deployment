#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

echo "Current working directory and its content:"
pwd
ls

bailout() {
    echo "Error encountered: Check if the files were found and if the diff command showed deviations"
    cd ..
    rm -rf check_config_stock_uptodate
    exit 1
}

mkdir -p check_config_stock_uptodate
cd check_config_stock_uptodate

wget -O orthanc.json https://salsa.debian.org/med-team/orthanc/-/raw/master/debian/configuration/orthanc.json?inline=false || bailout
diff orthanc.json ../roles/orthanc/templates/orthanc.json.stock || bailout

wget -O credentials.json https://salsa.debian.org/med-team/orthanc/-/raw/master/debian/configuration/credentials.json?inline=false || bailout
diff credentials.json ../roles/orthanc/templates/credentials.json.stock || bailout

wget -O openssl.cnf https://exampleconfig.com/static/raw/openssl/ubuntu20.04/etc/ssl/openssl.cnf || bailout
diff openssl.cnf ../roles/certificate/templates/openssl.cnf.stock || bailout

wget https://ftp.gnu.org/gnu/health/plugins/gnuhealth_plugin_crypto-latest.tar.gz || bailout
tar -xvf gnuhealth_plugin_crypto-latest.tar.gz || bailout
diff -r crypto/ ../roles/desktop/files/crypto/ || bailout

hg clone https://hg.savannah.gnu.org/hgweb/health-thalamus/ || bailout
diff ../roles/postgresql/files/federation_schema.sql health-thalamus/thalamus/demo/federation_schema.sql || bailout
diff ../roles/thalamus/templates/import_pg.py.stock health-thalamus/thalamus/demo/import_pg.py || bailout
diff ../roles/thalamus/templates/roles.cfg.stock health-thalamus/thalamus/etc/roles.cfg || bailout
diff ../roles/thalamus/templates/thalamus.cfg.stock health-thalamus/thalamus/etc/thalamus.cfg || bailout

#TODO: trytond.conf, pg_hba.conf. No scriptable source found yet.

echo "Templates and files are up to date"
cd ..
rm -rf check_config_stock_uptodate
