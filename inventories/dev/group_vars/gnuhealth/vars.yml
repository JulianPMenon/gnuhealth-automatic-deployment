# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

---
# Authentication
auth_os_user: "{{ vault_ssh_user }}"
auth_ssh_args: "{{ vault_ssh_args }}"
auth_ssh_password: "{{ vault_ssh_password }}"
auth_sudo_passwords: "{{ vault_sudo_pws }}"
auth_set_sudo_pass: "{{ vault_set_sudo_pass }}"

# Application
application: true
application_https: "{{ nginx_rproxy_domain != nginx_application_domain }}"

## GNU Health parameters
gh_version: 4.2.1
gh_psql_uri: "{{ gh_psql_uri_local if (psql_domain | default('localhost')) == (nginx_application_domain | default('localhost')) else gh_psql_uri_remote }}"
gh_demo_db: false
gh_webdav: false
gh_tryton_admin_mail: 'example@mail.com'
gh_modules:
  - health
  # - health_surgery
gh_update_weekly: false
gh_restart_service_after_update: false
# Want to connect to previously deployed Orthanc server with ownca cert?
gh_trust_cert_orthanc: false
gh_trust_certs:
  - ../fetch/orthanc-custom-ca.crt
gh_password: "{{ vault_gh_password | default('gnusolidario') }}"
gh_salt: "{{ vault_gh_salt | default('gnusaltidario') }}"
gh_tryton_pw: "{{ vault_tryton_pw | default('gnusolidario') }}"

## uWSGI parameters
uwsgi_config:
  min_workers: 1
  init_workers: 1
  max_workers: 42
  increase_workers: 1
  logging: true
  tcp_socket: "{{ nginx_rproxy_domain != nginx_application_domain and not application_https }}"
  unix_socket: "{{ nginx_rproxy_domain == nginx_application_domain }}"
  run_dir: "{{ gh_run_path }}"
uwsgi_instances:
  - "{{ uwsgi_hmis_instance }}"

# PostgreSQL
postgresql: true
psql_db_user: "gnuhealth"
psql_listen_remote: "{{ psql_domain != nginx_application_domain | default(false) }}"
psql_use_pw: "{{ psql_listen_remote }}"
psql_use_cert: "{{ psql_listen_remote }}"
psql_pw: "{{ vault_psql_pw | default('gnusolidario') }}"
psql_db_name: "health"
psql_db_template: "template1"
psql_domain: "{{ nginx_rproxy_domain | default('localhost') }}"
psql_hba_rules:
  - "local\t{{ psql_db_name }},{{ psql_db_template }}\t{{ psql_db_user }}\t\t\t\t\ttrust"  # DB system == appl system
  # - "hostssl\t{{ psql_db_name }},{{ gh_demo_db_name }},{{ psql_db_template }}\t{{ psql_db_user }}\t192.168.0.111/32\tscram-sha-256"  # DB system != appl sytem
psql_shared_buffers: "{{ (ansible_memtotal_mb * 0.4) | int | abs }}MB"
psql_encryption: 'scram-sha-256'
psql_tls_min: 'TLSv1.2'
psql_set_encoding: true
psql_encoding: 'UTF-8'
psql_set_locale: false
psql_locale: 'en_US'
psql_set_timezone: false
psql_timezone: 'Europe/Berlin'
psql_set_datestyle: false
psql_datestyle: 'iso, mdy'

# Nginx as reverse proxy
nginx: true
nginx_rproxy_domain: localhost  # for certificate & reverse proxy
nginx_application_domain: "{{ nginx_rproxy_domain }}"
nginx_http_redirect: false
nginx_site_hmis:
  - {name: 'GH_HMIS', internal_port: '{{ uwsgi_hmis_instance.port }}', external_port: '443', unix_socket_path: '{{ uwsgi_hmis_instance.unix_socket_path }}'}
nginx_site_webdav:
  - {name: 'GH_WebDAV', internal_port: '{{ gh_webdav_port }}', external_port: '8443', unix_socket_path: ''}
nginx_sites: "{{ gh_webdav | ternary ( nginx_site_hmis | union( nginx_site_webdav ), nginx_site_hmis ) }}"

# Certificates
cert_ownca:
  create_ca: true
  type: 'ownca'
  # cert_params for [country, state, town, organization, subsection, common name, filename without extension (.crt, .key)]
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit', "{{ inventory_hostname }}", "{{ groups['ownca'][0] if groups['ownca'] is defined and groups['ownca'] | length > 0 else inventory_hostname }}-CA"]
  cert_admin_mail: "{{ gh_tryton_admin_mail }}"
  revoke: []
    # - "{{ cert_ssl_paths.cert_path }}{{ cert_rproxy.cert_params[-1] }}.crt"
cert_rproxy:
  type: 'ownca'  # available types: 'ownca', 'copy' & 'letsencrypt'
  # only for 'ownca'
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit', '{{ nginx_rproxy_domain }}', 'gnuhealth-rproxy']
  renew: false
  # only for letsencrypt
  revoke: false
  # for 'ownca' and 'letsencrypt'
  cert_admin_mail: "{{ gh_tryton_admin_mail }}"
  # only for 'copy'
  # set remote_source true if cert + key are already on the remote system
  remote_source: false
  my_cert: "path/gnuhealth.crt"
  my_key: "path/gnuhealth.key"
cert_application:
  type: 'ownca'  # available types: 'ownca' or 'copy'
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit', '{{ nginx_application_domain }}', 'gnuhealth-appl']
  renew: false
  cert_admin_mail: "{{ gh_tryton_admin_mail }}"
  remote_source: false
  my_cert: "path/gnuhealth.crt"
  my_key: "path/gnuhealth.key"
cert_psql:
  type: 'ownca'  # available types: 'ownca' or 'copy'
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit', '{{ psql_domain }}', 'gnuhealth-psql']
  renew: false
  cert_admin_mail: "{{ gh_tryton_admin_mail }}"
  remote_source: false
  my_cert: "path/gnuhealth.crt"
  my_key: "path/gnuhealth.key"

# Miscellaneous
miscellaneous: true
msc_fail2ban: true
msc_fail2ban_banaction: "iptables-allports"
msc_fail2ban_use_mail: false
msc_security_admin_mail: "{{ gh_tryton_admin_mail }}"
# if msc_ssmtp is true, an existing dummy email is necessary for sending reports.
# if not, a working "sendmail" command is expected
msc_ssmtp: false
msc_ssmtp_mail_hub: "smtp.web.de:465"
msc_ssmtp_mail_address: "example@web.de"
msc_ssmtp_mail_pw: "{{ vault_mail_pw }}"
msc_email_on_service_inactivity: false
msc_monitor_services:
  - gnuhealth
  - nginx
  - fail2ban
  - postgresql
msc_automatic_updates: false
msc_automatic_reboot: false
msc_automatic_reboot_time: "02:00"
msc_set_timezone: '{{ psql_set_timezone }}'
msc_timezone: '{{ psql_timezone }}'
...
