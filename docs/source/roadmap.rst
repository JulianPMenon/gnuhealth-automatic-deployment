.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Roadmap
=======

**1.2**:
	- Add Backup/restore functionality
	- Extend test cases
**1.3**:
	- Add vagrant provider suitable for productive use
	- Set up server for molecule tests
**1.4**:
	- Monitoring functionality
	- Split miscellaneous & desktop into separate roles
