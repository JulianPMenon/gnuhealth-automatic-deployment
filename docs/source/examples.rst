.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Examples
========

The first two examples are meant to ease the usage of the scripts in order to set up a system for testing or developing.
The last examples can be used as a base for a productive system.
In that case please read the whole documentation and remember that this example is a minimal base that won't fulfill all your individual needs.

GNU Health server and client in one - Minimal example
-----------------------------------------------------
The first example is meant to be followed in an existing Virtual Machine (VM).
GNU Health server and client on one system, running Ansible locally on the same system, generating a custom CA:

* The user performing the deployment needs sudo access.

* Install requirements (first line Debian/Ubuntu, second openSUSE, afterwards same for both)::

    $ sudo apt install python3-pip mercurial
    $ sudo zypper install python3-pip mercurial
    $ pip3 install ansible-core
    $ nano ~/.bashrc
    add "export PATH=$PATH:/home/`whoami`/.local/bin", close (ctrl + x) & save
    $ source ~/.bashrc
    $ ansible-galaxy collection install community.crypto community.general community.postgresql

* Clone the repository and navigate inside its top level directory::

    $ hg clone https://hg.savannah.gnu.org/hgweb/health-ansible/
    $ cd health-ansible/

* Run the playbook gnuhealth.yml::

    $ ansible-playbook playbooks/gnuhealth.yml -i inventories/dev -c local -e auth_os_user=`whoami` -K

  You will be prompted for the sudo/become password thanks to the -K flag.
* Run the playbook desktop.yml::

    $ ansible-playbook playbooks/desktop.yml -i inventories/dev -c local -e auth_os_user=`whoami` -e ghcl_trust_certs=true -K

* Run the client either using the desktop entry or from terminal::

    $ gnuhealth-client

* In the client the connection informations are already taken from the previous installation thus just hit `Connect`

* Enter the password (`vault_tryton_pw` in inventories/<inventory>/group_vars/gnuhealth/vault.yml, 'gnusolidario' if not changed)

Create VirtualBox VMs using Vagrant - GNU Health - One system per service
-------------------------------------------------------------------------
Inside the `vagrant` folder there are Vagrantfiles for different scenarios.
Those are not only running Ansible but also creating the VMs using VirtualBox.
Choosing the subfolder `gnuhealth_split` spawns GNU Health split into three servers and one desktop system based on Ubuntu each having 1GB RAM.
The following installation procedure for the host was only tested on Ubuntu yet but should be similiar for other Linux distributions.
Install the requirements::

  $ sudo apt install python3-pip mercurial virtualbox sshpass wget
  $ wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  $ echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
  $ sudo apt update && sudo apt install vagrant
  $ pip3 install ansible-core
  $ export PATH=$PATH:/home/`whoami`/.local/bin
    # Add above line to .bashrc for permanent change
  $ ansible-galaxy collection install community.crypto community.general community.postgresql
  $ hg clone https://hg.savannah.gnu.org/hgweb/health-ansible/
  $ cd health-ansible/vagrant/gnuhealth_split
  $ vagrant up

This way you can easily test configurations used for separated systems. You will get four systems called `db`, `app`, `web` and `gui`.
If you want to connect to those VMs you can either use VirtualBox and click on `show`.
Or if you want to connect e.g. to the application GNU Health itself you can execute `vagrant ssh app` from the terminal in the same directory as above.

The Vagrantfile offers multiple choices for the server operating system but unfortunatelly no appropriate desktop boxes for Debian and openSUSE were found yet.

Productive use of GNU Health
----------------------------

This example includes a GNU Health server split into three subsystems for Nginx, GNU Health and PostgreSQL. Besides any number of client systems is deployed.
Those are meant to be a starting point but likely can not fulfill all your individual needs!

Preparation
~~~~~~~~~~~
First of all make sure:

- Your operating systems are supported (see Introduction in doubt)

- You can connect to all systems using SSH keys (see Securing SSH & PWs if you don't know how)

- The firewall rules allow only the desired access (see Firewall)

- Requirements were installed on the Ansible controller (see Requirements)

- Get official certificates for the Nginx, GNU Health and PostgreSQL server.
  If this is not applicable see Digital Certificates for handling the certificates in a different way.

- Before modifying any configuration or functionality of this repository save a copy of the whole folder as it was after cloning

Configuration
~~~~~~~~~~~~~
For this example we will directly start in the `prod` inventory.
You could use the `dev` inventory for modifying the functionality for your needs or analyzing different uWSGI configurations in terms of performance before.
The `test` inventory could be used for load and integration tests before moving to the final `prod` inventory.

First locate the desired inventory:

  $ cd /<your_path>/gnuhealth-automatic-deployment/inventories/prod/

Start by modifying `hosts` in the subfolder `inventory`:

- Outcomment any entry on top under "[gnuhealth]"

- Set domain names or IP addresses for your servers under "[gnuhealth_application]", "[gnuhealth_psql]" and "[gnuhealth_rproxy]"

- Set domain names or IP addresses for the clients under "[desktop]". Those can be given using ranges as indicated by the given outcommented lines.

Continue with credentials:

- Modify `vault.yml` in `group_vars/gnuhealth/`:

  - Enter your sudo user name for `vault_ssh_user`

  - If you did not enable passwordless sudo, set `vault_set_sudo_pass` to true and set the passwords below

  - Generate and set three new secure passwords for the operating system user `gnuhealth` (`vault_gh_password`), the `admin` user in Tryton (`vault_tryton_pw`) and the PostgreSQL user `gnuhealth` (`vault_psql_pw`)

- Modify `vault.yml` in `group_vars/desktop/`:

  - Enter your sudo user name for `vault_ssh_user`

  - If you did not enable passwordless sudo, set `vault_set_sudo_pass` to true and set the password below

- If you did not enable passwordless sudo, encrypt the vault.yml files using Ansible Vault::

  $ ansible-vault encrypt group_vars/gnuhealth/vault.yml
  $ ansible-vault encrypt group_vars/desktop/vault.yml

  Store those passwords securely, e.g. using KeePassXC or another password manager.

Next configure your GNU Health setup inside `group_vars/gnuhealth/vars.yml`:

- Set your domain names for `psql_domain`, `nginx_application_domain` and `nginx_rproxy_domain`

- Uncomment the second line for `psql_hba_rules` and set the IP address of your application server

- Set the desired modules for `gh_modules`.

- The configuration for uWSGI (`uwsgi_config`) depends on your individual setup, think of writing load tests for simulating your actual load and finding the best configuration. Or analyze your productive performance for different configurations afterwards.

- Set `cert_ownca.create_ca` to false

- For `cert_psql`, `cert_application` and `cert_rproxy` set paths to your certificate and key. If they are already on the target system, set `remote_source` to true.
  If they are on the Ansible controller they have to be readable during execution. In that case encrypt the private key using Ansible Vault::

    $ ansible-vault encrypt /path/to/key.pem

Then modify `vars.yml` in `group_vars/desktop/` as well:

- Set your hospital name for `ghcl_profile_name` as this will be displayed when opening the client.

- Set `msc_timezone` for your desired timezone and `msc_set_timezone` true

- If you want to verify the crypto plugin is working afterwards set `gpg_create_key` to true for creating a gpg key if you don't have one yet

Execution
~~~~~~~~~

Execute the playbook for the GNU Health server::

  $ ansible-playbook playbooks/gnuhealth.yml -i inventories/prod --ask-vault

If you encrypted both `vault.yml` and your key you need two prompts::

  $ ansible-playbook playbooks/gnuhealth.yml -i inventories/prod --vault-id vault@prompt --vault-id key@prompt

After successful execution run it again to check idempotence: All tasks should be `ok` and green.
If any task `changed` during the second execution idempotence was violated.

Next run the playbook for the clients::

  $ ansible-playbook playbooks/desktop.yml -i inventories/prod --ask-vault


Postprocessing
~~~~~~~~~~~~~~

Some functionalities are not part of the playbooks (yet).
Even if they are added in the future you should still adapt them to your specific needs.
The most important further steps are probably:

- Backup/Restore strategy

- Monitoring

- More extended IT security concept

You can likely extend your system by that components and still keep idempotency in the playbooks.

However if you want to have redundancy you should rather modify and extend the Ansible playbooks.
If you work on realizing redundancy or other extensions feel free to open merge requests or get in contact.
In that case - or if you have questions/feedback - you can write to wiese@gnuhealth.org

Productive use of GNU Health for very low budget
------------------------------------------------

As some implementations have very low resources regarding hardware and system administrators this example is meant as a base for a GNU Health server with all in one system and less experienced administrator:

- Prepare your server: Install the operating system and enable SSH during installation

- Follow the first example for requirements and getting the repository (without executing playbooks) on your system from which you will administrate the server

- Set the servers host name in `inventories/dev/inventory/hosts` on the top under `[gnuhealth]`

- Check if you can ping the server::

    $ ansible -m ping -i inventories/dev/ -e ansible_user=<username> gnuhealth

- Run the SSH playbook in order to switch SSH to key based authentication::

    $ ansible-playbook playbooks/ssh.yml -i inventories/dev -e ssh_username_remote=<username> -e ssh_username_local=`whoami` -K -k

- Add the local key to ssh-agent for future Ansible connections::

    $ ssh-agent bash
    $ ssh-add ~/.ssh/id_rsa
    $ exit

- Run the Firewall playbook in order to drop incoming traffic that is not HTTPS or SSH::

    $ ansible-playbook playbooks/firewall.yml -i inventories/dev -e fw_username_remote=<username> -K

- Set your servers host name in inventories/dev/group_vars/gnuhealth/vars.yml as value for `nginx_rproxy_domain`

- Run the GNU Health playbook in order to install PostgreSQL, GNU Health, uWSGI, Nginx & Fail2ban::

    $ ansible-playbook playbooks/firewall.yml -i inventories/dev -e auth_os_user=<username> -K

- If you want to test the server from your Ansible controller run the corresponding playbook locally::

    $ ansible-playbook playbooks/desktop.yml -i inventories/dev -c local -e auth_os_user=`whoami` -e ghcl_trust_certs=true -K
    $ gnuhealth-client

- For setting up the work stations they have to be accessible by SSH and their IP addresses or domain names entered in `hosts` under `[desktop]`. Having that given call the playbook like this::

    $ ansible-playbook playbooks/desktop.yml -i inventories/dev -e auth_os_user=<username> -e ghcl_trust_certs=true -K

- You could add more SSH keys for not being dependent from one. The SSH role also takes strings from inventories/dev/group_vars/ssh/vars.yml.
  Besides you can set `ssh_key_create` false if you already have a SSH key you want to use.

- The firewall role can also limit the source addresses to e.g. the hospital subnet (in inventories/dev/group_vars/firewall/vars.yml)

- Realize automatic backups for the attachments folder /opt/gnuhealth/var/lib/ and a database dump, test the restore

- Think of some basic monitoring / health checking routines
