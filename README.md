<!--
SPDX-FileCopyrightText: 2023 Gerald Wiese

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Automatic Deployment of the open source hospital information system GNU Health using Ansible
The Ansible playbooks in this project aim to automatically deploy the hospital information system GNU Health including the HMIS node, the DICOM server Orthanc and a desktop environment as a client using the GNU Health HMIS node.  
**HMIS node**: Hospital management information system, core of the GNU Health system  
**Orthanc**: The DICOM server is not directly part of the GNU Health system but its integration is provided  
**Thalamus**: GNU Health Federation (Development in progress)  
Supported operating systems are Ubuntu 20.04 & 22.04 LTS, openSUSE Leap 15.4 and Debian 11.  
Find the full documentation here:  
https://geraldwiese.gitlab.io/gnuhealth-automatic-deployment/

# Main functionality
The main steps of the playbooks for GNU Health are the following:
- Install PyPI package
- Configure uWSGI
- Create systemd service(s)
- Install and configure PostgreSQL & Nginx
- Create database user and database
- Create a reverse proxy
- Create or handle digital certificates
- Install the GNU Health client on any number of machines, enter the servers connection information meanwhile

This project is not intended to work in a dockerized environment but is rather expected to be used in Virtual Machines.
If you want different systems for different services it is possible to separate PostgreSQL, Nginx and a custom Certificate Authority from the actual application GNU Health.

For a quickstart jump to the section 'Examples' in the full documentation (link on top).

# Links
Ansible documentation https://docs.ansible.com/  
GNU Health Documentation Portal https://www.gnuhealth.org/docs/  
Orthanc Book https://book.orthanc-server.com/  
PostgreSQL documentation https://www.postgresql.org/docs/  
Nginx documentation https://nginx.org/en/docs/  
Fail2Ban https://www.fail2ban.org/  
