.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Welcome to GNU Health - Automatic Deployment's documentation!
=============================================================

.. toctree::
   :maxdepth: 1
   :numbered:
   :caption: Contents:

   introduction
   requirements
   structure
   check
   cert
   psql
   gnuhealth
   uwsgi
   nginx
   miscellaneous
   desktop
   orthanc
   thalamus
   examples
   vault
   firewall
   ssh
   backup
   tests
   roadmap
   trouble
   license
