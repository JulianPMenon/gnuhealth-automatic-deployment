.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Thalamus
========

The GNU Health Federation aims to create a network of multiple HMIS servers.
Thalamus is the backend to create this federation.
It is still in development and no frontend exists yet to e.g. show statistics.

The installation strategy is very similar to the HMIS in terms of the directory structure, pip installation inside a virtual environment and putting uWSGI in front despite that `gnuhealth` is replaced by `thalamus`.

| You can find the documentation of Thalamus here:
| https://www.gnuhealth.org/docs/thalamus/
