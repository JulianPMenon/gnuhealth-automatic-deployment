.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Firewall
========
It's a good idea to only open the ports you really need - especially for incoming traffic.
In the most simple setup of one server containing all services you probably want 22 & 443 open for SSH & HTTPS and deny everything else.
On GNU/Linux systems this can be accomplished using iptables.

This repository contains a very simple firewall role & playbook that realizes that.
The playbook allows established connections which are answers to outgoing connections.
Besides traffic from the loopback range is allowed in order not to break local services like systemd-resolv.
Finally open ports and target sources can be configured.
By default those are 22 & 443 for all IPv4 addresses.

If you want to run the playbook for an all in one server using the default configuration simply execute this command::

    $ ansible-playbook playbooks/firewall.yml -i inventories/dev -e fw_username_remote=<username> -K -k

The firewall role and playbook are not meant to get very complex as firewall needs are very individual and on multiple layers anyway.
If you want to extend its functionality for distributed servers you should probably move some differing variables from `group_vars` to `host_vars`.
You can read more about `host_vars` here:

https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#organizing-host-and-group-variables

Apart from SSH you would at least want this allowed:

- PostgreSQL:
    - Allow 5432 ingress, only coming from GNU Health

- GNU Health:
    - Allow 5432 egress, only to PostgreSQL
    - Allow 8443 ingress, only coming from Nginx

- Nginx:
    - Allow 8443 egress, only to GNU Health
    - Allow 443 (& 80) ingress, only coming from your hospital (sub-)network
