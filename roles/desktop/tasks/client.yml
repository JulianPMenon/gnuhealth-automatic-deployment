# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

- name: Include hmis vars
  ansible.builtin.include_vars:
    file: "{{ ghcl_hmis_vars_file }}"
  when: ghcl_include_hmis_vars

- name: Set python gpg package for different Leap versions
  ansible.builtin.set_fact:
    pythongpg:
      '15.3': python-gnupg
      '15.4': python3-gpg
  when: ansible_facts.os_family == 'Suse'
- name: Install gpg and gnuhealth-client
  community.general.zypper:
    name: [gpg, "{{ pythongpg[ansible_facts['distribution_version']] }}", "gnuhealth-client=={{ ghcl_version }}"]
  when: ansible_facts.os_family == 'Suse'

- name: Install gnuhealth-client using debian/ubuntu
  when: ansible_facts.os_family == 'Debian'
  block:
    - name: Install pip, gpg, venv and PyGObject dependencies
      ansible.builtin.package:
        name: "{{ ghcl_packages[ansible_facts.os_family] }}"
    - name: Install gnuhealth-client and python-gnupg with pip inside venv
      ansible.builtin.pip:
        virtualenv: /opt/gnuhealth_client
        name:
          - "gnuhealth-client=={{ ghcl_version }}"
          - python-gnupg
        extra_args: "{{ '--index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple/' if ghcl_use_testpypi else '' }}"
    - name: Create a link for the gnuhealth-client
      ansible.builtin.file:
        src: /opt/gnuhealth_client/bin/gnuhealth-client
        dest: /usr/local/bin/gnuhealth-client
        state: link
        owner: "{{ ansible_user }}"
        group: "{{ ansible_user }}"
        mode: '0755'
        follow: false

- name: Create gnuhealth plugins and gnuhealth-clients config directory
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: '0755'
  loop:
    - "/etc/skel/.config/gnuhealth/{{ ghcl_version[:3] }}/"
    - /opt/gnuhealth_plugins
- name: Create a link for the gnuhealth plugins
  ansible.builtin.file:
    src: /opt/gnuhealth_plugins
    dest: "{{ item }}"
    state: link
    follow: false
  loop:
    - "/etc/skel/.config/gnuhealth/{{ ghcl_version[:3] }}/plugins"
    - "/etc/skel/gnuhealth_plugins"

- name: Copy crypto plugin
  ansible.builtin.copy:
    src: crypto/
    dest: /opt/gnuhealth_plugins/gnuhealth_crypto_plugin_dir
    mode: '0755'

- name: Apply profiles.cfg template
  ansible.builtin.template:
    src: profiles.cfg.j2
    dest: /etc/skel/.config/gnuhealth/{{ ghcl_version[:3] }}/profiles.cfg
    mode: '0644'
- name: Apply gnuhealth-client.conf template
  ansible.builtin.template:
    src: gnuhealth-client.conf.j2
    dest: /etc/skel/.config/gnuhealth/{{ ghcl_version[:3] }}/gnuhealth-client.conf
    mode: '0644'
- name: Copy config from /etc/skel to users home directory
  ansible.builtin.copy:
    src: /etc/skel/.config/gnuhealth/
    dest: "/home/{{ ansible_user }}/.config/gnuhealth"
    owner: "{{ ansible_user }}"
    group: "{{ 'users' if ansible_facts.os_family == 'Suse' else ansible_user }}"
    remote_src: true
    mode: '0755'
  when: ghcl_copy_config
- name: Check if link already exists
  ansible.builtin.stat:
    path: "/home/{{ ansible_user }}/gnuhealth_plugins"
  register: ghcl_plugins
  when: ghcl_copy_config
- name: Create a link for the gnuhealth plugins in current home directory as well
  ansible.builtin.file:
    src: /opt/gnuhealth_plugins
    dest: "/home/{{ ansible_user }}/gnuhealth_plugins"
    state: link
    follow: false
  when: ghcl_copy_config and not ghcl_plugins.stat.exists

- name: Copy .desktop file
  ansible.builtin.copy:
    src: 'gnuhealth-client.desktop'
    dest: '/usr/share/applications/'
    mode: '0644'
  when: ansible_facts.os_family == 'Debian'
- name: Copy icon for desktop entry
  ansible.builtin.copy:
    src: 'gnuhealth.png'
    dest: '/opt/'
    mode: '0644'
  when: ansible_facts.os_family == 'Debian'
