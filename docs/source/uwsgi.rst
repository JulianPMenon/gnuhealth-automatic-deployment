.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

uWSGI
====================

| uWSGI is an open source web server that implements the Python Web Server Gateway Interface (WSGI, see PEP 3333).
| The use of uWSGI gives many configuration options like a dynamic amount of workers which increases the robustness for productive use.
| There are three connection methods between uWSGI and Nginx:

- If Nginx is on the same system a UNIX socket will be used.
  Therefore a group `uwsginginx` is created and both `gnuhealth` & `ẁwww-data` have to be part of that group.
  Otherwise you would either have a world-writable socket or could not access your server.

- Besides the uwsgi protocol can be used by Nginx if systems are separated but the intranet considered secure.

- Alternatively uWSGI can also provide HTTPS.

The uWSGI configuration contains two parts: Necessary steps for getting it running and recommendations for productive use of this source:

https://www.bloomberg.com/company/stories/configuring-uwsgi-production-deployment/

Configuration options in `vars.yml` are prefixed by `uwsgi_`. Those are mainly for the handling of workers.
Furthermore you have default entries `uwsgi_hmis_instance` and `uwsgi_th_instance` which are already designed to get uWSGI running with GNU Health HMIS Node and Thalamus.
They can be found in the roles `defaults` folder.

The template `uwsgi.ini.j2` contains many configuration options that were already commented inside.
Worth to note is the harakiri option: It kills workers if a request exceeds the threshold of currently 600s.
This is intended to prevent having numerous idle workers but could raise problems as well.

Finally uWSGI will be managed by systemd: A .service file will be copied and the service enabled so it always starts after booting.

| uWSGI documentation:
| https://uwsgi-docs.readthedocs.io/en/latest/
