#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

echo "Current working directory and its content:"
pwd
ls

echo "Linting gnuhealth.yml"
ansible-lint -p playbooks/gnuhealth.yml

echo "Linting desktop.yml"
ansible-lint -p playbooks/desktop.yml

echo "Linting orthanc.yml"
ansible-lint -p playbooks/orthanc.yml

echo "Linting thalamus.yml"
ansible-lint -p playbooks/thalamus.yml

echo "Linting ssh.yml"
ansible-lint -p playbooks/ssh.yml

echo "Linting firewall.yml"
ansible-lint -p playbooks/firewall.yml

echo "No problems found during linting"
