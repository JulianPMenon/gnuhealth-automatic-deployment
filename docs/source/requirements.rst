.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Requirements
============
- Hardware: The software itself does not have big needs. You can test it with Virtual Machines having 15 GB storage and 1 GB RAM. For productive use it depends on the size and load.

- The installation user needs to be part of the sudo group (usually the case).

- Install pip and mercurial, then clone the repository::

    $ sudo apt install python3-pip mercurial
    $ hg clone https://hg.savannah.gnu.org/hgweb/health-ansible/

- Install ansible::

    $ pip3 install ansible-core
    $ export PATH=$PATH:/home/`whoami`/.local/bin

  To make changes permanent add the last line to ~/.bashrc::

    $ nano ~/.bashrc (insert line, close and save)
    $ source ~/.bashrc

  Install ansible collections::

    $ ansible-galaxy collection install community.crypto community.general community.postgresql

- In case of remote access:

  - Preferably setup a SSH key (see Securing SSH & PWs) or otherwise install sshpass where you call the playbooks::

      $ sudo apt install sshpass

  - Make sure the remote machines are accessible via SSH, e.g. using very basic firewall::

      $ sudo apt install openssh-server
      $ sudo ufw allow ssh

- If you run the playbook on the target system you need neither sshpass nor openssh-server but have to set
  the connection local when running the playbook, for example::

    $ ansible-playbook playbooks/gnuhealth.yml -c local -i inventories/dev -e auth_os_user=`whoami` -K

- If using openSUSE replace apt by zypper for installing packages.

- In order to use Vagrant and VirtualBox check how to install those on your operating system.
